#ifndef TASK_HPP
#define TASK_HPP

#include <ostream>

class Task {
  private:
    unsigned computer_col;
    unsigned human_col;
    double result = 0;

  public:
    Task(unsigned computer_col, unsigned human_col);
    Task();

    auto get_computer_col() -> const unsigned&;
    auto get_human_col() -> const unsigned&;

    auto get_result() -> double;
    auto set_result(double result) -> void;

    auto friend operator<<(std::ostream& os, const Task& task)
        -> std::ostream&;
};

#endif
