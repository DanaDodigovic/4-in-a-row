#ifndef COMPUTER_HPP
#define COMPUTER_HPP

#include "Player.hpp"
#include "Tasks.hpp"
#include "Communication.hpp"

#include <variant>

class Computer : public Player {
  private:
    int n_workers        = 0;
    int n_active_workers = 0;
    bool over            = false;

  public:
    Computer(Board& board, int depth, int argc, char* argv[]);
    ~Computer();

    auto do_move() -> void override;
    auto has_won() -> bool override;

  private:
    auto get_best_column(const std::vector<double> results) -> unsigned;
    auto notify_about_end() -> void;
    auto send_board() -> void;
    auto send_task_or_wait(MPI_Status status, const std::optional<Task>& task)
        -> void;
    auto store_result(const std::pair<Task, double>& result,
                      std::vector<double>& results) -> void;
    auto receive_message(MPI_Status* status)
        -> std::variant<Action, std::pair<Task, double>>;
    auto get_average_score(const std::vector<double>& results, unsigned column)
        -> double;
};

#endif
