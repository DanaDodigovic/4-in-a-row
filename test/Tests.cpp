#include <catch2/catch.hpp>
#include <iostream>

#include "Board.hpp"
#include "Computer.hpp"
#include "Player.hpp"
#include "Tasks.hpp"
#include "Worker.hpp"

TEST_CASE("Vertical test")
{
    Board board;

    board.do_move(Token::Human, 0);
    board.do_move(Token::CPU, 0);
    board.do_move(Token::CPU, 0);
    board.do_move(Token::CPU, 0);
    board.do_move(Token::CPU, 0);

    REQUIRE(board.test_victory());
}

TEST_CASE("Horizontal test")
{
    Board board;

    board.do_move(Token::CPU, 1);
    board.do_move(Token::CPU, 2);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::CPU, 4);

    REQUIRE(board.test_victory());
}

TEST_CASE("Diagonal main test")
{
    Board board;

    board.do_move(Token::CPU, 0);
    board.do_move(Token::Human, 1);
    board.do_move(Token::CPU, 1);
    board.do_move(Token::Human, 2);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::Human, 2);
    board.do_move(Token::CPU, 2);
    board.do_move(Token::Human, 3);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::Human, 4);
    board.do_move(Token::CPU, 3);

    REQUIRE(board.test_victory());
}

TEST_CASE("Diagonal main test 2")
{
    Board board;

    board.do_move(Token::CPU, 5);
    board.do_move(Token::Human, 6);
    board.do_move(Token::CPU, 6);

    REQUIRE(!board.test_victory());
}

TEST_CASE("Diagonal main test 3")
{
    Board board;

    board.do_move(Token::CPU, 6);
    board.do_move(Token::Human, 1);
    board.do_move(Token::CPU, 1);
    board.do_move(Token::Human, 2);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::Human, 2);
    board.do_move(Token::CPU, 2);
    board.do_move(Token::Human, 3);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::Human, 4);
    board.do_move(Token::CPU, 4);
    board.do_move(Token::Human, 4);
    board.do_move(Token::CPU, 4);
    board.do_move(Token::Human, 6);
    board.do_move(Token::CPU, 4);
    board.do_move(Token::Human, 6);
    board.do_move(Token::CPU, 3);

    REQUIRE(board.test_victory());
}

TEST_CASE("Diagonal side test")
{
    Board board;

    board.do_move(Token::CPU, 5);
    board.do_move(Token::Human, 4);
    board.do_move(Token::CPU, 4);
    board.do_move(Token::Human, 3);
    board.do_move(Token::CPU, 2);
    board.do_move(Token::Human, 3);
    board.do_move(Token::CPU, 3);
    board.do_move(Token::Human, 1);
    board.do_move(Token::CPU, 2);
    board.do_move(Token::Human, 2);
    board.do_move(Token::CPU, 2);

    REQUIRE(board.test_victory());
}
