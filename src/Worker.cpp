#include "Worker.hpp"
#include "Communication.hpp"

#include <limits>

Worker::Worker(int depth) : current_task({0, 0}), depth(depth) {}

auto Worker::do_work() -> void
{
    MPI_Status status;

    while (!over) {
        // Receive board or 'end' message.
        receive_board_or_end();

        while (!over) {
            send_request(); // Send request for task.
            auto response = receive_task_or_wait(&status);

            // 'wait' message is received.
            if (has_to_wait(status, response)) break;

            // Task is received.
            process_task(std::get<Task>(response));
        }
    }
}

auto Worker::evaluate_move(bool cpu_turn, int depth) -> double
{
    // Last turn was !cpu_turn.
    if (my_board.test_victory()) { return cpu_turn ? -1 : 1; }
    if (depth == 0) { return 0; }

    double total                 = 0;
    unsigned legal_moves         = 0;
    bool all_succerssors_winners = true, all_succerssors_losers = true;

    for (unsigned c = 0; c < my_board.get_ncols(); ++c) {
        if (!my_board.is_move_legal(c)) { continue; }

        ++legal_moves;

        my_board.do_move(cpu_turn ? Token::CPU : Token::Human, c);
        current_task.set_result(evaluate_move(!cpu_turn, depth - 1));
        my_board.undo_move(c);

        if (current_task.get_result() != -1) { all_succerssors_losers = false; }
        if (current_task.get_result() != 1) { all_succerssors_winners = false; }
        if (current_task.get_result() == 1 && !cpu_turn) { return 1; }
        if (current_task.get_result() == -1 && cpu_turn) { return -1; }

        total += current_task.get_result();
    }

    if (all_succerssors_winners) { return 1; }
    if (all_succerssors_losers) { return -1; }

    return legal_moves ? total / legal_moves :
                         -std::numeric_limits<double>::infinity();
}

auto Worker::is_done() -> bool
{
    return over;
}

auto Worker::process_task(Task task) -> void
{
    std::variant<Action, std::pair<Task, double>> result;
    if (task.get_result() == -std::numeric_limits<double>::infinity()) {
        result = std::make_pair(task, task.get_result());
        Communication::send(result, 0, Tag::Result);
        return;
    }

    unsigned comp_col  = task.get_computer_col();
    unsigned human_col = task.get_human_col();

    my_board.do_move(Token::CPU, comp_col);
    if (my_board.test_victory()) {
        result = std::make_pair(task, 1.0);
        my_board.undo_move(comp_col);
        Communication::send(result, 0, Tag::Result);
        return;
    }

    my_board.do_move(Token::Human, human_col);
    if (my_board.test_victory()) {
        result = std::make_pair(task, -1.0);
        my_board.undo_move(human_col);
        my_board.undo_move(comp_col);
        Communication::send(result, 0, Tag::Result);
        return;
    }

    result = std::make_pair(task, evaluate_move(true, depth));

    my_board.undo_move(human_col);
    my_board.undo_move(comp_col);

    Communication::send(result, 0, Tag::Result);
}

auto Worker::receive_board_or_end() -> void
{
    MPI_Status status;

    auto msg = Communication::receive<std::variant<Action, Board>>(
        0, Tag::Any, &status);

    if (status.MPI_TAG == static_cast<int>(Tag::Board)) {
        my_board = std::get<Board>(msg);
    } else if (status.MPI_TAG == static_cast<int>(Tag::Action)) {
        auto action = std::get<Action>(msg);

        if (action == Action::End) { over = true; }
    }
}

auto Worker::send_request() -> void
{
    std::variant<Action, double> request = Action::TaskRequest;
    Communication::send(request, 0, Tag::TaskRequest);
}

auto Worker::receive_task_or_wait(MPI_Status* status)
    -> std::variant<Task, Action>
{
    return Communication::receive<std::variant<Task, Action>>(
        0, Tag::Any, status);
}

auto Worker::has_to_wait(MPI_Status status, std::variant<Task, Action> response)
    -> bool
{
    if (status.MPI_TAG == static_cast<int>(Tag::Action)) {
        auto action = std::get<Action>(response);
        if (action == Action::Wait) { // No available tasks.
            return true;              // Wait for board or end again.
        }
    }
    return false;
}
