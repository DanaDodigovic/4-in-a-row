#include "Task.hpp"

Task::Task(unsigned computer_col, unsigned human_col)
    : computer_col(computer_col), human_col(human_col)
{}

Task::Task() {}

auto Task::get_computer_col() -> const unsigned&
{
    return computer_col;
}

auto Task::get_human_col() -> const unsigned&
{
    return human_col;
}

auto Task::get_result() -> double
{
    return result;
}

auto Task::set_result(double result) -> void
{
    this->result = result;
}

auto operator<<(std::ostream& os, const Task& task) -> std::ostream&
{
    return os << '(' << task.computer_col << ", " << task.human_col << ')';
}
