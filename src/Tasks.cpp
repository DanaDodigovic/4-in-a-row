#include "Tasks.hpp"

#include <iostream>
#include <limits>

Tasks::Tasks(const Board& board) : board(board)
{
    for (size_t i = 0; i < board.get_ncols(); ++i) {
        for (size_t j = 0; j < board.get_ncols(); ++j) {
            tasks.emplace_back(i, j);
        }
    }
}

auto Tasks::get_task() -> std::optional<Task>
{
    if (last_given == tasks.size()) { return std::optional<Task>(); }

    unsigned comp_col  = tasks[last_given].get_computer_col();
    unsigned human_col = tasks[last_given].get_human_col();

    if (comp_col == human_col) {
        // Check if there are two available tokens in the same column.
        if (board.get_n_of_free_tokens(comp_col) < 2) { // Forbidden task.
            tasks[last_given].set_result(
                -std::numeric_limits<double>::infinity());
            return tasks[last_given++];
        }
    }

    // Allowed task.
    if (board.has_free_tokens(comp_col) && board.has_free_tokens(human_col)) {
        return tasks[last_given++];
    } else { // Forbidden task.
        tasks[last_given].set_result(-std::numeric_limits<double>::infinity());
        return tasks[last_given++];
    }
}

auto Tasks::get_number() const -> unsigned
{
    return tasks.size();
}

auto Tasks::get_average_score(unsigned column) -> double
{
    double score = 0;
    unsigned n   = 0;

    for (unsigned i = board.get_ncols() * column;
         i < board.get_ncols() * (column + 1);
         ++i) {
        if (tasks[i].get_result() == -1) { return -1; }
        if (tasks[i].get_result() != -std::numeric_limits<double>::infinity()) {
            score += tasks[i].get_result();
            ++n;
        }
    }

    return n ? score / n : -std::numeric_limits<double>::infinity();
}

auto Tasks::empty() -> bool
{
    return !active_tasks;
}
