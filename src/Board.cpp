#include "Board.hpp"

#include <iostream>

auto Board::do_move(Token token, unsigned column) -> void
{
    // Find first free row in a given column and mark it with provided token.
    for (unsigned i = 0; i < nrows; ++i) {
        if (tokens[i][column] == Token::Free) {
            last_row          = i;
            tokens[i][column] = token;
            break;
        }
    }

    last_column = column;
    last_played = token == Token::CPU ? Token::CPU : Token::Human;
}

auto Board::undo_move(unsigned column) -> void
{
    // Find last inserted row in a given column and mark it as free.
    for (int i = nrows - 1; i >= 0; --i) {
        if (tokens[i][column] != Token::Free) {
            tokens[i][column] = Token::Free;
            break;
        }
    }
}

auto Board::test_victory() -> bool
{
    if (last_played == Token::Free) {
        throw std::runtime_error("Illegal field token!");
    }

    auto column    = get_column(last_column);
    auto row       = get_row(last_row);
    auto main_diag = get_diagonal(last_row, last_column, true);
    auto side_diag = get_diagonal(last_row, last_column, false);

    return test_stripe(column) || test_stripe(row) || test_stripe(main_diag) ||
           test_stripe(side_diag);
}

auto Board::is_full() -> bool
{
    for (unsigned c = 0; c < ncols; ++c) {
        if (has_free_tokens(c)) { return false; }
    }
    return true;
}

auto Board::has_free_tokens(unsigned column) const -> bool
{
    // At least one field in a column is free.
    if (tokens[nrows - 1][column] == Token::Free)
        return true;
    else
        return false;
}

auto Board::get_n_of_free_tokens(unsigned column) const -> unsigned
{
    // Find first free row in a given column.
    for (size_t i = 0; i < nrows; ++i) {
        if (tokens[i][column] == Token::Free) { return nrows - i; }
    }
    return 0;
}

auto Board::is_move_legal(unsigned column) -> bool
{
    return get_n_of_free_tokens(column) >= 1 ? true : false;
}

auto operator<<(std::ostream& os, const Board& board) -> std::ostream&
{
    for (int i = board.nrows - 1; i >= 0; --i) {
        for (size_t j = 0; j < board.ncols; j++) {
            if (board.tokens[i][j] == Token::Free) {
                os << "=";
            } else if (board.tokens[i][j] == Token::CPU) {
                os << "C";
            } else {
                os << "P";
            }
        }
        os << "\n";
    }
    return os;
}

auto Board::get_column(unsigned c) const -> std::array<Token, nrows>
{
    std::array<Token, nrows> column;
    for (unsigned i = 0; i < nrows; ++i) { column[i] = tokens[i][c]; }
    return column;
}

auto Board::get_row(unsigned r) const -> std::array<Token, ncols>
{
    return tokens[r];
}

auto Board::get_diagonal(unsigned r, unsigned c, bool main) const
    -> std::array<Token, std::min(nrows, ncols)>
{
    std::array<Token, std::min(nrows, ncols)> diag;
    for (auto& field : diag) { field = Token::Free; }

    if (main) {
        unsigned offset  = std::min(r, c);
        unsigned start_r = r - offset;
        unsigned start_c = c - offset;

        for (unsigned i = start_r, j = start_c, k = 0; i < nrows && j < ncols;
             ++i, ++j, ++k) {
            diag[k] = tokens[i][j];
        }
    } else {
        unsigned offset  = std::min(r, ncols - c);
        unsigned start_r = r - offset;
        unsigned start_c = c + offset;

        for (unsigned i = start_r, j = start_c, k = 0; i < nrows && j > 0;
             ++i, --j, ++k) {
            diag[k] = tokens[i][j];
        }
    }

    return diag;
}

auto Board::get_n_of_free_columns() const -> unsigned
{
    unsigned count = 0;
    for (unsigned i = 0; i < ncols; ++i) {
        if (tokens[nrows - 1][i] == Token::Free) { ++count; }
    }

    return count;
}
